package com.ssg.demoaks;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * It's a simple controller for testing purposes
 */
@RestController
public class HelloController {
   
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
}